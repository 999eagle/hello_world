# Hello, world!

A simple hello world program written in C. Should work on any x86_64 Linux system. Might kill your cat on any other OS.

## Running

Compile with `gcc hello_world.c -nostdlib -o hello_world` and run with `./hello_world`.

## What the heck is happening here?

See this post by me for a full explanation: <https://999eagle.moe/posts/hello-world/>
